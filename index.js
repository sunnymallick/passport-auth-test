const express = require('express');
const session = require('express-session')
const passport = require('passport')
require('./auth')

const isLoggedIn = (req, res, next) => req.user ? next() : res.sendStatus(401)

const app = express();
app.use(express.json())
app.use(session({secret: 'cats'}));

//initializes the req.passport necessary which is used all over
app.use(passport.initialize());
//to persist login sessions
app.use(passport.session())

app.get('/', (req, res) => {
    res.write('<a href="/auth/google">Authenticate with Google <br>')
    res.write('<a href="/auth/facebook">Authenticate with Facebook <br>')
    res.write('<a href="/auth/apple">Authenticate with Apple <br>')
    res.send()
})

app.get('/auth/google',
    passport.authenticate('google', {
        scope: [
            'email',
            'profile',
            'https://www.googleapis.com/auth/contacts',
            'https://www.googleapis.com/auth/drive'
        ]
    })
);

app.get('/google/callback',
    passport.authenticate('google', {
        successRedirect: '/protected',
        failureRedirect: '/auth/failure'
    })
)

app.get('/auth/facebook',
    passport.authenticate('facebook', {
        scope: [
            'email',
            'public_profile'
        ]
    })
)

app.get('/facebook/callback',
    passport.authenticate('facebook', {
        successRedirect: '/protected',
        failureRedirect: '/auth/failure'
    })
)

app.get('/auth/apple',
    passport.authenticate('apple')
)

app.post('/auth', (req, res, next) => {
    passport.authenticate('apple', function(err, user, info) {
        if (err) {
            if (err === "AuthorizationError") {
                res.send("Oops! Looks like you didn't allow the app to proceed. Please sign in again! <br /> \
                <a href=\"/login\">Sign in with Apple</a>");
            } else if (err === "TokenError") {
                res.send("Oops! Couldn't get a valid token from Apple's servers! <br /> \
                <a href=\"/login\">Sign in with Apple</a>");
            }
        } else {
            res.json(user);
        }
    })(req, res, next);
})

app.get('/protected', isLoggedIn, (req, res) => {
    res.send(`We've been waiting for you ${req.user.displayName}! <br> <a href='/logout'>Logout ${req.user.displayName}?`)
})

app.get('/logout', (req, res) => {
    req.logout();
    req.session.destroy()
    res.send(`You have been logged out! <br> <a href='/'>Return Home`)
})

app.get('/auth/failure', (req, res) => {
    res.send('Your login has failed')
})

app.listen(3000, () =>
    console.log('Listening on port 3000'))