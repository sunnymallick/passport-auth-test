const passport = require('passport')
const config = require('./config')
const GoogleStrategy = require( 'passport-google-oauth20' ).Strategy;
const FacebookStrategy = require('passport-facebook').Strategy
const AppleStrategy = require('passport-apple');

const GOOGLE_CLIENT_ID = config.google.google_client_id;
const GOOGLE_CLIENT_SECRET = config.google.google_client_secret;
const FACEBOOK_APP_ID = config.facebook.facebook_app_id;
const FACEBOOK_APP_SECRET = config.facebook.facebook_app_secret;

//Google
passport.use(new GoogleStrategy({
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: GOOGLE_CLIENT_SECRET,
        callbackURL: "http://localhost:3000/google/callback",
        passReqToCallback: true
    },
    function(request, accessToken, refreshToken, profile, done) {
        return done(null, profile)
    }
));

//Facebook
passport.use(new FacebookStrategy({
        clientID: FACEBOOK_APP_ID,
        clientSecret: FACEBOOK_APP_SECRET,
        callbackURL: "http://localhost:3000/facebook/callback"
    },
    function(accessToken, refreshToken, profile, done) {
        return done(null, profile)
    }
));

//Apple
passport.use(new AppleStrategy({
    clientID: "io.redskytech.test-sign",
    teamID: "QQPD9BB4K6",
    callbackURL: "https://sign-in.redskytech.io/auth/apple/return",
    keyID: "UKT2RWRG28",
    passReqToCallback: true
},
    function(req, accessToken, refreshToken, idToken, profile, done) {
        return done(null, profile)
    }
    ));

//creates cookie for the browser
passport.serializeUser((user, done) => {
    done(null, user)
});

//extracts
passport.deserializeUser((user, done) => {
    done(null, user)
});